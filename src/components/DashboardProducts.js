import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  useDeleteProductMutation,
  useActivateProductMutation,
  useDeactivateProductMutation,
} from '../services/appApi';
import '../pages/Styles.css';
import Pagination from './Pagination';

function DashboardProducts() {
  const products = useSelector((state) => state.products);
  const user = useSelector((state) => state.user);
  // removing the product
  const [deleteProduct, { isLoading, isSuccess }] = useDeleteProductMutation();
  const [activateProduct, { isLoading: activateLoading }] =
    useActivateProductMutation();
  const [deactivateProduct, { isLoading: deactivateLoading }] =
    useDeactivateProductMutation();

  function handleDeleteProduct(id) {
    //  logic here
    if (window.confirm('Are you sure?'))
      deleteProduct({ product_id: id, user_id: user._id });
  }

  function TableRow({ pictures, _id, name, price, isActive }) {
    return (
      <tr>
        <td>
          <img
            src={pictures[0].url}
            className="dashboard-product-preview"
            alt="dashboard"
          />
        </td>
        <td>{_id}</td>
        <td>{name}</td>
        <td>{price}</td>
        <td>
          <Link to={`/product/${_id}/edit`} className="btn btn-warning">
            Edit
          </Link>
          <Button
            onClick={() =>
              isActive
                ? deactivateProduct({ id: _id })
                : activateProduct({ id: _id })
            }
            disabled={isLoading}
          >
            {isActive ? 'Deactivate' : 'Activate'}
          </Button>
          <Button className="btn btn-danger"
            onClick={() => handleDeleteProduct(_id, user._id)}
            disabled={isLoading}
          >
            Delete
          </Button>
        </td>
      </tr>
    );
  }

  return (
    <Table striped bordered hover responsive>
      <thead>
        <tr>
          <th></th>
          <th>Product ID</th>
          <th>Product Name</th>
          <th>Product Price</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <Pagination
          data={products}
          RenderComponent={TableRow}
          pageLimit={1}
          dataLimit={10}
          tablePagination={true}
        />
      </tbody>
    </Table>
  );
}

export default DashboardProducts;
