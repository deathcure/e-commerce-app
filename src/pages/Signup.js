// Import dependencies
import React, { useState } from 'react';
import { Alert, Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useSignupMutation } from '../services/appApi';
import { useNavigate } from 'react-router-dom';
import './Styles.css';

function Signup() {
  // Define state variables to store email and password
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [signup, { error, isLoading, isError }] = useSignupMutation();
  const navigate = useNavigate();

  function handleSignup(e) {
    e.preventDefault();
    signup({ name, email, password })
      .unwrap()
      .then(() => {
        navigate('/login', { replace: true });
      })
      .catch(() => {});
  }

  return (
    <Container>
      <Row>
        <Col md={6} className="signup__form--container">
          <Form style={{ width: '100%' }} onSubmit={handleSignup}>
            {/* Signup form */}
            <h1>Create an account</h1>
            {isError && <Alert variant="danger">{error.data}</Alert>}
            {/* Name input */}
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter your name"
                value={name}
                required
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>

            {/* Email input */}
            <Form.Group>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                required
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            {/* Password input */}
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter Password"
                value={password}
                required
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>

            {/* Signup button */}
            <Form.Group>
              <Button type="submit" disabled={isLoading}>
                Create Account
              </Button>
            </Form.Group>

            {/* Login account link */}
            <p>
              Already have an account? <Link to="/login">Login</Link>{' '}
            </p>
          </Form>
        </Col>
        {/* Signup image */}
        <Col md={6} className="signup__image--container"></Col>
      </Row>
    </Container>
  );
}

export default Signup;
