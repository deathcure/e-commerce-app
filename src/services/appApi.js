// Import necessary functions from Redux Toolkit Query
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

// Create the API
export const appApi = createApi({
  // Define the name of the API's reducer slice in the store
  reducerPath: 'appApi',
  // Define the base URL for all API requests
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.REACT_APP_API_URL}` }),
  // Define the API's endpoints and their configurations
  endpoints: (builder) => ({
    // Define the 'signup' endpoint as a mutation that makes a POST request to '/users/signup'
    signup: builder.mutation({
      query: (user) => ({
        url: '/users/signup',
        method: 'POST',
        body: user,
      }),
    }),
    // Define the 'login' endpoint as a mutation that makes a POST request to '/users/login'
    login: builder.mutation({
      query: (user) => ({
        url: '/users/login',
        method: 'POST',
        body: user,
      }),
    }),
    // Create a product
    createProduct: builder.mutation({
      query: (product) => ({
        url: '/products',
        body: product,
        method: 'POST',
      }),
    }),
    // Delete a product
    deleteProduct: builder.mutation({
      query: ({ product_id, user_id }) => ({
        url: `/products/${product_id}`,
        body: {
          user_id,
        },
        method: 'DELETE',
      }),
    }),
    // Update a Product
    updateProduct: builder.mutation({
      query: (product) => ({
        url: `/products/${product.id}`,
        body: product,
        method: 'PATCH',
      }),
    }),
    // Activate a Product
    activateProduct: builder.mutation({
      query: (product) => ({
        url: `/products/${product.id}/activate`,
        method: 'PATCH',
      }),
    }),
    // Deactivate a Product
    deactivateProduct: builder.mutation({
      query: (product) => ({
        url: `/products/${product.id}/deactivate`,
        method: 'PATCH',
      }),
    }),
    // Add to cart
    addToCart: builder.mutation({
      query: (cartInfo) => ({
        url: '/products/add-to-cart',
        body: cartInfo,
        method: 'POST',
      }),
    }),
    // Remove from cart
    removeFromCart: builder.mutation({
      query: (body) => ({
        url: '/products/remove-from-cart',
        body,
        method: 'POST',
      }),
    }),
    // Increase cart
    increaseCartProduct: builder.mutation({
      query: (body) => ({
        url: '/products/increase-cart',
        body,
        method: 'POST',
      }),
    }),
    // Decrease cart
    decreaseCartProduct: builder.mutation({
      query: (body) => ({
        url: '/products/decrease-cart',
        body,
        method: 'POST',
      }),
    }),
    // Create Order
    createOrder: builder.mutation({
      query: (body) => ({
        url: '/orders',
        method: 'POST',
        body,
      }),
    }),
  }),
});

// Export hooks that can be used to call the 'signup' and 'login' mutations
export const {
  useSignupMutation,
  useLoginMutation,
  useCreateProductMutation,
  useAddToCartMutation,
  useRemoveFromCartMutation,
  useIncreaseCartProductMutation,
  useDecreaseCartProductMutation,
  useCreateOrderMutation,
  useDeleteProductMutation,
  useUpdateProductMutation,
  useActivateProductMutation,
  useDeactivateProductMutation,
} = appApi;

// Export the API for use in the application
export default appApi;
