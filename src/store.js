// Import necessary libraries and modules needed to create the Redux store.
import { configureStore } from '@reduxjs/toolkit';
import productSlice from './features/productSlice';
import userSlice from './features/userSlice';
import appApi from './services/appApi';

// Import library to persist the Redux store and configure it.
import storage from 'redux-persist/lib/storage';
import { combineReducers } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';

// Combine all the reducers used in the application.
const reducer = combineReducers({
  user: userSlice,
  products: productSlice,
  [appApi.reducerPath]: appApi.reducer,
});

// Configure the persistence settings for the Redux store.
const persistConfig = {
  key: 'root',
  storage,
  blacklist: [appApi.reducerPath], //inalis ko products
};

// Create a new reducer with the persistence settings.
const persistedReducer = persistReducer(persistConfig, reducer);

// Create the Redux store with the new reducer and necessary middleware.
const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk, appApi.middleware],
});

// Export the Redux store as the default export of this module.
export default store;
