// This is an import statement that imports the createSlice function from the @reduxjs/toolkit library.
import { createSlice } from '@reduxjs/toolkit';

// appApi
// Imports the appApi function from the ../services/appApi file.
import appApi from '../services/appApi';

// Creates a constant variable initialState and initializes it to an empty array.
const initialState = [];

// Creates a slice of the Redux store called productSlice using the createSlice function
// Reducers property is an empty object, indicating that no reducers are defined for this slice.
export const productSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    updateProducts: (_, action) => {
      return action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      appApi.endpoints.createProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.updateProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.deleteProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.activateProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.deactivateProduct.matchFulfilled,
      (_, { payload }) => payload
    );
  },
});

export const { updateProducts } = productSlice.actions;
// Update the state in response to actions dispatched to the Redux store
export default productSlice.reducer;
